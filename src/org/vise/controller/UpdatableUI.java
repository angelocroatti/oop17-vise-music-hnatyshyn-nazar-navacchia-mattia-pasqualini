package org.vise.controller;

/**
 *
 */
public interface UpdatableUI {

    /**
     * Show a notificaton.
     * @param textNotification
     *          The text of the notification.
     */
    void showNotification(String textNotification);
}
